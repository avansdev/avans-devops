﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avans_Devops
{
    public class GitIntegration
    {
        public void Pull() { }
        public void Push() { }
        public void Commit(string BranchName) { }
        public void NewBranch(string BranchName) { }
        public void Stash() { }
        public void Pop() { }
    }
}
